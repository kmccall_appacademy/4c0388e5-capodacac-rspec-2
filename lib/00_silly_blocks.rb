###
#
# SILLY BLOCKS
# App Academy - Prep-cohort
# Christopher Stenqvist, cc0
# 07/29/17
#
###




require "byebug"

def reverser(&str)
	words = str.call
	words = words.split(" ")
	i=0
	while(i<words.length)
    words[i] =	reverse_word(words[i]) 
		i=i+1
	end
	words = words.join(" ")
	words
end

def reverse_word(str)
  stackme = ""
  word = str
  len = word.length
  i=0
  while(i<len)
		stackme = pop(word).to_s + stackme.to_s
    i = i + 1
  end
  stackme
end

def adder(num1=0, &num2)
  n = num2.call

	if(num1==0)
	  n = n+1
	else
    n = num1+num2.call
	end
	n
end

def repeater(loops=0, &lmda)
	if( loops==0 )
		return lmda.call
	end

  i=0
	while(i<loops)
	  lmda.call
		i = i + 1
	end
end

def pop(arr)
  first = arr[0,1]
  arr[0,1] = ""
  first
end

def popback(word)
  temp = pop(word)
  word = word + temp
  word
end
