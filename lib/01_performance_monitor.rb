###
#
# PERFORMANCE MONITOR
# App Academy - Prep-cohort
# Christopher Stenqvist, cc0
# 07/29/17
#
###
require "byebug"
require "time"


def measure(limit=1,&proc)
	start = Time.now
	limit.times do
   proc.call
	end
  ending = Time.now

  (ending - start)/limit

end





